import React, { FC, HTMLAttributes, ReactChild } from 'react';
import styled from 'styled-components';
export interface Props extends HTMLAttributes<HTMLDivElement> {
  /** custom content, defaults to 'the snozzberries taste like snozzberries' */
  children?: ReactChild;
  label?: string
}

// Please do not use types off of a default export module or else Storybook Docs will suffer.
// see: https://github.com/storybookjs/storybook/issues/9556
/**
 * A custom Thing component. Neat!
 */
export const Thing: FC<Props> = ({ children, label }) => {
  return( <div>{children || `the snozzberries taste like snozzberries`}
    <StyledDiv>{label || 'Aloha'}</StyledDiv>
  </div>);
};

const StyledDiv = styled.div`
background-color: pink;
color: blue;
`;